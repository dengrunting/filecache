import requests
import re
from bs4 import BeautifulSoup


def get_list_page():
    url = "https://www.cfmem.com/search/label/free"
    soup = BeautifulSoup(requests.get(url).text, "html.parser")
    durl = soup.find(name="h2").find("a").get("href")

    soup = BeautifulSoup(requests.get(durl).text, "html.parser")
    ret_list = soup.find("h2", string=re.compile(r"订阅链接")).next_sibling.find_all("div")
    
    ret_map  = {}
    for ret in ret_list:
        n, u = ret.text.split("：")
        if n and u:
            ret_map[n] = u
    return ret_map


def main():
    ret_map = get_list_page()
    print(ret_map)

if __name__ == "__main__":
    main()